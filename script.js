document.querySelector(".btn-change-color").addEventListener("click",(event) => {
    event.preventDefault();
    document.body.classList.toggle("dark");
    let body = document.querySelector("body");
    localStorage.setItem("scheme",body.classList.contains("dark") ? "dark" : "main");
});
document.addEventListener("DOMContentLoaded",() => {
    let scheme = localStorage.getItem("scheme");
    if (scheme === "dark") {
        document.body.classList.add("dark");
    }
});
